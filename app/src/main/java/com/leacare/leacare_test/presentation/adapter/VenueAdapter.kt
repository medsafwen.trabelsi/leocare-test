package com.leacare.leacare_test.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.leacare.leacare_test.databinding.AdapterVenueItemBinding
import com.leacare.leacare_test.domain.models.remote.VenuesData

/**
 * Venue adapter.
 */
class VenueAdapter : ListAdapter<VenuesData, VenueViewHolder>(DIFF_UTIL) {

    /** Item click listener. */
    var deleteItemClickListener: ((VenuesData) -> Unit)? = null

    /** Delete item click listener. */
    var onItemClickListener: ((VenuesData) -> Unit)? = null

    override fun getItemCount() = currentList.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueViewHolder {
        return VenueViewHolder(
            AdapterVenueItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        ).apply {
            itemView.setOnClickListener { onItemClickListener?.invoke(getItem(adapterPosition)) }

            deleteItemButton.setOnClickListener {
                deleteItemClickListener?.invoke(getItem(adapterPosition))
            }

        }
    }

    override fun onBindViewHolder(holder: VenueViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<VenuesData>() {
            override fun areItemsTheSame(oldItem: VenuesData, newItem: VenuesData): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: VenuesData, newItem: VenuesData): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}

/**
 * Venue view holder.
 */
class VenueViewHolder(
    private val binding: AdapterVenueItemBinding
) : RecyclerView.ViewHolder(binding.root) {
    val deleteItemButton = binding.venueDelete
    fun bind(holder: VenuesData) {
        binding.data = holder
        binding.executePendingBindings()
    }
}