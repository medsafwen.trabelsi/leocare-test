package com.leacare.leacare_test.presentation.fragments

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.androidmapsextensions.GoogleMap
import com.androidmapsextensions.Marker
import com.androidmapsextensions.MarkerOptions
import com.androidmapsextensions.SupportMapFragment
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.leacare.leacare_test.R
import com.leacare.leacare_test.core.InjectedViewModelFactory
import com.leacare.leacare_test.core.util.isLocationPermissionsGranted
import com.leacare.leacare_test.core.util.showToast
import com.leacare.leacare_test.databinding.FragmentHomeBinding
import com.leacare.leacare_test.domain.models.remote.VenuesData
import com.leacare.leacare_test.presentation.activities.VenueDetailsActivity
import com.leacare.leacare_test.presentation.viewmodels.HomeViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * Home fragment.
 */
@SuppressLint("MissingPermission")
class HomeFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: InjectedViewModelFactory

    /** Views binding. */
    private var _binding: FragmentHomeBinding? = null

    /** Views binding. */
    private val binding: FragmentHomeBinding
        get() = _binding!!

    /** Google maps instance. */
    private var googleMap: GoogleMap? = null

    /** Fused location client. */
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    /** Location request client. */
    private lateinit var locationRequest: LocationRequest

    /** Home view model. */
    private lateinit var viewModel: HomeViewModel

    /** Selected venue. */
    private var selectedVenue = ""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentView: View
        FragmentHomeBinding.inflate(inflater, container, false).apply {
            fragmentView = root
            _binding = this



            activity?.let {
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(it)
            }

            initMap()

            homeVenueDetails.setOnClickListener {
                startActivity(
                    VenueDetailsActivity.intent(
                       context,
                        selectedVenue
                    )
                )
            }

            homeMyLocationButton.setOnClickListener {
                if (requireContext().isLocationPermissionsGranted(this@HomeFragment)) {
                    getLastPositionAndMoveCamera()
                }
            }
            refreshDataButton.setOnClickListener {
                val latitude = googleMap?.cameraPosition?.target?.latitude
                val longitude = googleMap?.cameraPosition?.target?.longitude
                if (latitude != null && longitude != null) {
                    getVenuesList(latitude, longitude)
                }
            }
            viewModel = ViewModelProvider(
                this@HomeFragment,
                viewModelFactory
            ).get(HomeViewModel::class.java)


        }


        return fragmentView
    }

    /**
     * Initialise map.
     */
    private fun initMap() {
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 20 * 1000

        (childFragmentManager.findFragmentById(R.id.home_map_fragment) as? SupportMapFragment)?.getExtendedMapAsync {
            googleMap = it

            googleMap?.uiSettings?.let { uiSettings ->
                uiSettings.isMyLocationButtonEnabled = false
                uiSettings.isMapToolbarEnabled = false
                uiSettings.isRotateGesturesEnabled = false
            }

            if (requireContext().isLocationPermissionsGranted(this@HomeFragment)) {
                getLastPositionAndMoveCamera()
            }

            googleMap?.setOnMapClickListener {
                binding.homeMyLocationButton.isVisible = true
                binding.homeVenueDetails.isVisible = false
            }

            googleMap?.setOnMarkerClickListener { marker: Marker? ->
                return@setOnMarkerClickListener when (marker) {
                    null -> false
                    else -> {
                        selectedVenue = (marker.getData() as VenuesData).id
                        binding.data = (marker.getData() as VenuesData)
                        binding.executePendingBindings()
                        binding.homeMyLocationButton.isVisible = false
                        binding.homeVenueDetails.isVisible = true
                        true
                    }
                }
            }
        }
    }


    /**
     * Change map camera.
     */
    private fun changeMapCamera(
            latitude: Double,
            longitude: Double
    ) {
        googleMap?.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(latitude, longitude),
                CURRENT_COUNTRY_ZOOM
            ), object : GoogleMap.CancelableCallback {
                override fun onFinish() {
                }

                override fun onCancel() {
                }
            })
    }


    /**
     * Get venues list.
     */
    private fun getVenuesList(latitude: Double, longitude: Double) {
        lifecycleScope.launch(Dispatchers.Main) {
            googleMap?.clear()
            viewModel.getVenues(latitude, longitude)?.forEach {
                it?.let {
                    val marker = MarkerOptions()
                        .data(it)
                        .title(it.name)
                        .position(LatLng(it.latitude, it.longitude))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_1))
                    googleMap?.addMarker(marker)
                }
            }
        }
    }

    /**
     * Get last location and move camera.
     */
    private fun getLastPositionAndMoveCamera() {
        googleMap?.isMyLocationEnabled = true
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let { it ->
                    changeMapCamera(it.latitude, it.longitude)
                    getVenuesList(it.latitude, it.longitude)
                }
            }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_LOCATION_PERMISSION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    getLastPositionAndMoveCamera()
                } else {
                    requireContext().showToast(getString(R.string.location_information_message))
                }
                return
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        /** Default maps zoom.*/
        private const val CURRENT_COUNTRY_ZOOM = 15f

        /** Location permission request code.*/
        const val REQUEST_CODE_LOCATION_PERMISSION = 100
    }

}