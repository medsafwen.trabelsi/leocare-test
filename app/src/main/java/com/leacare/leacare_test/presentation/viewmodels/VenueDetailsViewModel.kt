package com.leacare.leacare_test.presentation.viewmodels

import androidx.lifecycle.ViewModel
import com.leacare.leacare_test.domain.models.remote.VenueDetailsData
import com.leacare.leacare_test.domain.usescases.AddVenueUseCase
import com.leacare.leacare_test.domain.usescases.FetchVenueDetailUsesCase
import javax.inject.Inject

/**
 * VenueDetailsModel view model.
 */
class VenueDetailsViewModel @Inject constructor(
    /** fetch venue details use case. */
    private var fetchVenueDetailUsesCase: FetchVenueDetailUsesCase,

    /** fetch venue details use case. */
    private var addVenueUseCase: AddVenueUseCase,
) : ViewModel() {

    /**
     *  Fetch venues details.
     */
    suspend fun getVenueDetails(venueId: String): VenueDetailsData? {
        return fetchVenueDetailUsesCase.invoke(venueId)
    }

    /**
     *  Add favorite venue.
     */
    suspend fun addFavoriteVenue(venueDetailsData: VenueDetailsData) {
        return addVenueUseCase.invoke(venueDetailsData)
    }
}