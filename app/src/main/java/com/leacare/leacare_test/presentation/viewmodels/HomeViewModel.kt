package com.leacare.leacare_test.presentation.viewmodels

import androidx.lifecycle.ViewModel
import com.leacare.leacare_test.domain.models.remote.VenuesData
import com.leacare.leacare_test.domain.usescases.FetchVenuesUsesCase
import javax.inject.Inject

/**
 * HomeViewModel view model.
 */
class HomeViewModel @Inject constructor(
    /** Fetch venues use case. */
    private var fetchVenuesUsesCase: FetchVenuesUsesCase,
) : ViewModel() {

    /**
     *  Get venues from api.
     */
    suspend fun getVenues(latitude: Double, longitude: Double): List<VenuesData?>? {
        return fetchVenuesUsesCase.invoke(latitude,longitude)
    }

}