package com.leacare.leacare_test.presentation.viewmodels


import androidx.lifecycle.ViewModel
import com.leacare.leacare_test.data.models.local.models.VenueEntity
import com.leacare.leacare_test.domain.usescases.DeleteVenueUseCase
import com.leacare.leacare_test.domain.usescases.GetVenuesUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * FavoriteVenueViewModel view model.
 */
class FavoriteVenueViewModel @Inject constructor(
    /** Delete venues use case. */
    private var deleteVenueUsesCase: DeleteVenueUseCase,
    /** Get all venues use case. */
    private var getVenuesUseCase: GetVenuesUseCase
) : ViewModel() {


    /**
     * Delete venue
     * */
    suspend fun getAllVenues(): Flow<List<VenueEntity>> {
        return getVenuesUseCase.invoke()
    }

    /**
     * Delete venue
     * */
    suspend fun deleteVenue(venueId: String) {
        deleteVenueUsesCase.invoke(venueId)
    }
}