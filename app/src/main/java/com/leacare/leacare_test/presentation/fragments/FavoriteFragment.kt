package com.leacare.leacare_test.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.leacare.leacare_test.R
import com.leacare.leacare_test.core.InjectedViewModelFactory
import com.leacare.leacare_test.data.models.toVenue
import com.leacare.leacare_test.databinding.FragmentFavoriteBinding
import com.leacare.leacare_test.presentation.activities.VenueDetailsActivity
import com.leacare.leacare_test.presentation.adapter.VenueAdapter
import com.leacare.leacare_test.presentation.common.FullMarginDecorator
import com.leacare.leacare_test.presentation.extension.setVerticalLinearLayoutManager
import com.leacare.leacare_test.presentation.viewmodels.FavoriteVenueViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Favorite fragment.
 */

class FavoriteFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: InjectedViewModelFactory

    /** Favorite view model. */
    private lateinit var viewModel: FavoriteVenueViewModel

    /** Venue adapter. */
    private lateinit var venueAdapter: VenueAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentView: View
        FragmentFavoriteBinding.inflate(inflater, container, false).apply {
            fragmentView = root
            viewModel = ViewModelProvider(
                this@FavoriteFragment,
                viewModelFactory
            ).get(FavoriteVenueViewModel::class.java)

            venueAdapter = VenueAdapter()


            lifecycleScope.launch {
                viewModel.getAllVenues().collect {
                    venueAdapter.submitList(it.map {
                        it.toVenue()
                    })
                    data = it.size
                    executePendingBindings()

                }

            }

            venueRecyclerView.setVerticalLinearLayoutManager()
            venueRecyclerView.addItemDecoration(FullMarginDecorator(R.dimen.margin_large))
            venueRecyclerView.adapter = venueAdapter

            venueAdapter.deleteItemClickListener = {
                lifecycleScope.launch(Dispatchers.Main) {
                    viewModel.deleteVenue(it.id)
                }
            }

            venueAdapter.onItemClickListener = {
                startActivity(VenueDetailsActivity.intent(requireContext(), it.id))
            }

        }
        return fragmentView
    }
}