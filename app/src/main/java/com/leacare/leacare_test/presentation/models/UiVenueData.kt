package com.leacare.leacare_test.presentation.models

/**
 * [UiVenueData], with formatted  ui data.
 */
data class UiVenueData(
    /** Venue id. */
    val id: String,

    /** Venue name. */
    val name: String,

    /** Venue short name. */
    val shortName: String,

    /** Venue url image. */
    val urlImage: String
)
