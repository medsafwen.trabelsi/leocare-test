package com.leacare.leacare_test.presentation.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.leacare.leacare_test.R
import com.leacare.leacare_test.core.InjectedViewModelFactory
import com.leacare.leacare_test.core.util.showToast
import com.leacare.leacare_test.databinding.ActivityVenueDetailsBinding
import com.leacare.leacare_test.domain.models.remote.VenueDetailsData
import com.leacare.leacare_test.presentation.viewmodels.VenueDetailsViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Venue details activity.
 */
class VenueDetailsActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: InjectedViewModelFactory

    /** Venue details view model. */
    private lateinit var viewModel: VenueDetailsViewModel

    /** Venue details view model. */
    private var currentVenues = VenueDetailsData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ActivityVenueDetailsBinding.inflate(layoutInflater).apply {
            setContentView(root)

            toolbar.setNavigationOnClickListener { finish() }



            viewModel = ViewModelProvider(this@VenueDetailsActivity, viewModelFactory).get(
                VenueDetailsViewModel::class.java
            )
            intent.getStringExtra(EXTRA_VENUE_ID)?.let {
                lifecycleScope.launch(Dispatchers.Main) {
                    viewModel.getVenueDetails(it)?.let {
                        currentVenues = it
                        data = currentVenues
                        executePendingBindings()
                    } ?: finish()
                }
            } ?: finish()

            addToFavorite.setOnClickListener {
                lifecycleScope.launch(Dispatchers.Main) {
                    viewModel.addFavoriteVenue(currentVenues)
                    showToast(getString(R.string.venue_successfully_added))
                }
            }

        }
    }

    companion object {

        /** Intent extra: venue id. */
        private const val EXTRA_VENUE_ID = "venueId"

        /**
         * Creates an intent to the venue details activity.
         */
        fun intent(context: Context?, venueId: String): Intent {
            return Intent(context, VenueDetailsActivity::class.java).apply {
                putExtra(EXTRA_VENUE_ID, venueId)
            }
        }
    }
}