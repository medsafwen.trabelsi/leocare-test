package com.leacare.leacare_test.presentation.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.leacare.leacare_test.R
import com.leacare.leacare_test.databinding.ActivityMainBinding


/**
 * Application main activity. Hosts most of the interesting fragments (home, favorite).
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityMainBinding.inflate(layoutInflater).apply {
            setContentView(root)

            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.main_navigation_host_fragment) as NavHostFragment

            NavigationUI.setupWithNavController(mainNavigationView, navHostFragment.navController)
        }
    }
}