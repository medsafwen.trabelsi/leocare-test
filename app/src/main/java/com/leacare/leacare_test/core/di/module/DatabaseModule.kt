package com.leacare.leacare_test.core.di.module

import android.content.Context
import androidx.room.Room
import com.leacare.leacare_test.data.models.local.LeacareTestDataBase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Database related injection module.
 */
@Module
class DatabaseModule {
    /**
     * Provides the local database.
     */
    @Provides
    @Singleton
    fun provideDatabase(context: Context): LeacareTestDataBase {
        val databaseBuilder = Room.databaseBuilder(context, LeacareTestDataBase::class.java, "LeacareTestDataBase-database")
        return databaseBuilder.build()
    }
}