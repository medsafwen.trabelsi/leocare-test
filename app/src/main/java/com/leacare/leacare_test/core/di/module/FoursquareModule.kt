package com.leacare.leacare_test.core.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.leacare.leacare_test.core.InjectedViewModelFactory
import com.leacare.leacare_test.core.di.annotations.ViewModelKey
import com.leacare.leacare_test.data.repositories.FoursquareRepositoryImpl
import com.leacare.leacare_test.data.service.FoursquareRetrofitService
import com.leacare.leacare_test.domain.repositories.FoursquareRepository
import com.leacare.leacare_test.presentation.activities.MainActivity
import com.leacare.leacare_test.presentation.activities.VenueDetailsActivity
import com.leacare.leacare_test.presentation.fragments.FavoriteFragment
import com.leacare.leacare_test.presentation.fragments.HomeFragment
import com.leacare.leacare_test.presentation.viewmodels.FavoriteVenueViewModel
import com.leacare.leacare_test.presentation.viewmodels.HomeViewModel
import com.leacare.leacare_test.presentation.viewmodels.VenueDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit

/**
 * "Foursquare module" app module binding module.
 */
@Module(includes = [FoursquareModule.Declarations::class])
class FoursquareModule {
    /**
     * Provides a [FoursquareRetrofitService] instance.
     */
    @Provides
    fun provideFoursquareRetrofitService(
            okHttpClientBuilder: OkHttpClient.Builder
    ): FoursquareRetrofitService {

        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.foursquare.com/v2/")
                .client(okHttpClientBuilder.build())
                .addConverterFactory(Json {
                    ignoreUnknownKeys = true

                }.asConverterFactory("application/json".toMediaType()))
                .build()

        return retrofit.create(FoursquareRetrofitService::class.java)
    }

    @Module
    internal interface Declarations {
        // region Activities

        @ContributesAndroidInjector
        abstract fun bindMainActivity(): MainActivity

        @ContributesAndroidInjector
        abstract fun bindVenueDetailsActivity(): VenueDetailsActivity

        // endregion

        // region fragments
        @ContributesAndroidInjector
        abstract fun bindHomeFragment(): HomeFragment

        @ContributesAndroidInjector
        abstract fun bindFavoriteFragment(): FavoriteFragment
        // endregion

        // region Repositories

        @Binds
        abstract fun bindStationRepository(impl: FoursquareRepositoryImpl): FoursquareRepository


        /**
         * ViewModel factory providing the ability to inject fields in ViewModel implementations.
         */
        @Binds
        abstract fun bindViewModelFactory(viewModelFactory: InjectedViewModelFactory): ViewModelProvider.Factory

        @Binds
        @IntoMap
        @ViewModelKey(HomeViewModel::class)
        abstract fun provideHomeViewModel(viewModel: HomeViewModel): ViewModel

        @Binds
        @IntoMap
        @ViewModelKey(VenueDetailsViewModel::class)
        abstract fun provideVenueDetailsViewModel(viewModel: VenueDetailsViewModel): ViewModel

        @Binds
        @IntoMap
        @ViewModelKey(FavoriteVenueViewModel::class)
        abstract fun provideFavoriteVenueViewModel(viewModel: FavoriteVenueViewModel): ViewModel
    }
}