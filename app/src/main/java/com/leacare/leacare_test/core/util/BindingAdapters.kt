package com.leacare.leacare_test.core.util

import android.view.View
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.leacare.leacare_test.R

/**
 * Load image url.
 */

@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: String?) {
    Glide.with(view.context).load(url ?: "").error(R.drawable.ic_default_image).into(view)
}

/**
 * Changes the provided view visibility.
 */
@BindingAdapter("isVisible")
fun setVisible(view: View, isVisible: Boolean) {
    view.isVisible = isVisible
}