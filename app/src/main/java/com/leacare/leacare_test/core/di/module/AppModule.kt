package com.leacare.leacare_test.core.di.module

import android.app.Application
import android.content.Context
import com.leacare.leacare_test.core.LeacareTestApplication
import dagger.Binds
import dagger.Module

@Module
abstract class AppModule {
    @Binds
    abstract fun bindsApplicationContext(application: LeacareTestApplication?): Context

    @Binds
    abstract fun bindsApplication(application: Application?): Application
}