package com.leacare.leacare_test.core.di

import com.leacare.leacare_test.core.LeacareTestApplication
import com.leacare.leacare_test.core.di.module.AppModule
import com.leacare.leacare_test.core.di.module.DatabaseModule
import com.leacare.leacare_test.core.di.module.FoursquareModule
import com.leacare.leacare_test.core.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        FoursquareModule::class,


    ]
)
internal interface AppComponent : AndroidInjector<LeacareTestApplication> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: LeacareTestApplication): AppComponent
    }
}