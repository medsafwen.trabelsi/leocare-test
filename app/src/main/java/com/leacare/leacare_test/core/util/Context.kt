package com.leacare.leacare_test.core.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.leacare.leacare_test.presentation.fragments.HomeFragment.Companion.REQUEST_CODE_LOCATION_PERMISSION

// region Permissions

/**
 * Check and request location permission.
 *
 * @return true when location permission is granted be called, false otherwise.
 */
fun Context.isLocationPermissionsGranted(fragment: Fragment): Boolean {
    var hasPermission = false
    if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
    ) {
        hasPermission = true
    } else {
        fragment.requestPermissions(
                arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ), REQUEST_CODE_LOCATION_PERMISSION
        )

    }
    return hasPermission
}


// endregion

// region Toast

/**
 * Show toast.
 */
fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

// endregion
