package com.leacare.leacare_test.data.repositories

import com.leacare.leacare_test.data.models.ERROR_UNKNOWN
import com.leacare.leacare_test.data.models.local.models.VenueEntity
import com.leacare.leacare_test.data.models.remote.Resource
import com.leacare.leacare_test.data.models.remote.Success
import com.leacare.leacare_test.data.models.toVenueDetails
import com.leacare.leacare_test.data.models.toVenuesData
import com.leacare.leacare_test.data.source.FoursquareLocalDataSource
import com.leacare.leacare_test.data.source.FoursquareRemoteDataSource
import com.leacare.leacare_test.domain.models.remote.VenueDetailsData
import com.leacare.leacare_test.domain.models.remote.VenuesData
import com.leacare.leacare_test.domain.repositories.FoursquareRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * FoursquareRepository implementation.
 * */
class FoursquareRepositoryImpl @Inject constructor(
    private val foursquareRemoteDataSource: FoursquareRemoteDataSource,
    private val foursquareLocalDataSource: FoursquareLocalDataSource
) : FoursquareRepository {


    override suspend fun fetchVenues(
        latitude: Double,
        longitude: Double
    ): Resource<List<VenuesData?>?> =
        when (val result = foursquareRemoteDataSource.fetchVenues(latitude, longitude)) {
            is Error -> Resource.error(ERROR_UNKNOWN)

            is Success -> {
                Resource.success(result.data.venuesList?.map { it.toVenuesData() })
            }

            else -> Resource.error(ERROR_UNKNOWN)
        }

    override suspend fun fetchVenueDetails(venueId: String): Resource<VenueDetailsData?> {
        return when (val result = foursquareRemoteDataSource.fetchVenueDetails(venueId)) {
            is Error -> Resource.error(ERROR_UNKNOWN)

            is Success -> {
                Resource.success(result.data.venueDetails.toVenueDetails())
            }

            else -> Resource.error(ERROR_UNKNOWN)
        }
    }

    override suspend fun getVenues(): Flow<List<VenueEntity>> {
        return foursquareLocalDataSource.getVenueData()
    }

    override suspend fun insertVenue(venueEntity: VenueEntity) {
        return foursquareLocalDataSource.insertVenueData(venueEntity)
    }

    override suspend fun deleteVenue(venueId: String) {
        return foursquareLocalDataSource.deleteVenueData(venueId)
    }
}