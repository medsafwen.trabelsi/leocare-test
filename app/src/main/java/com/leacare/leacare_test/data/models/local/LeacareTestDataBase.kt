package com.leacare.leacare_test.data.models.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.leacare.leacare_test.data.models.local.dao.VenueDao
import com.leacare.leacare_test.data.models.local.models.VenueEntity

/**
 * Main application database.
 */
@Database(
    entities = [
        VenueEntity::class,
    ],
    version = 1,
    exportSchema = true
)
abstract class LeacareTestDataBase : RoomDatabase() {
    abstract fun venueDao(): VenueDao

}
