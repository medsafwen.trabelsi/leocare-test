package com.leacare.leacare_test.data.models.remote

import java.io.InvalidObjectException

/**
 * A generic class that holds a value with its loading status.
 *
 * See https://github.com/android/architecture-components-samples/blob/master/GithubBrowserSample/app/src/main/java/com/android/example/github/vo/Resource.kt
 */
sealed class Resource<out T> {
    /** Flag indicating that the current resource is a SUCCESS. */
    val isSuccess: Boolean
        get() = (this is Success)

    /** Flag indicating that the current resource is an ERROR. */
    val isError: Boolean
        get() = (this is Error)

    /** Flag indicating that the current resource is a SUCCESS. */
    val isFinished: Boolean
        get() = when (this) {
            is Success, is Error -> true
        }

    /** Error code, 0 for non ERROR statuses. */
    open val errorCode: Int = 0

    /** Data. Can only be used for SUCCESS status. */
    open val data: T
        get() = throw InvalidObjectException("No data value for the resource $this")

    /** Data. Can be used for ALL statuses. Returns null for non SUCCESS statuses. */
    open val dataOrNull: T? = null

    companion object {
        /**
         * @return a SUCCESS Resource.
         */
        fun <T> success(data: T): Resource<T> {
            return Success(data)
        }


        /**
         * @return an ERROR Resource, with optional error code and message.
         */
        fun <T> error(errorCode: Int = 0): Resource<T> {
            return Error(errorCode)
        }
    }
}

/**
 * Resource, for a SUCCESS status.
 */
class Success<out T>(
    /** Internal result data. */
    private val internalData: T
) : Resource<T>() {
    override val data: T
        get() = internalData

    override val dataOrNull: T?
        get() = internalData
}


/**
 * Resource, for a ERROR status. Can NOT contain result data.
 */
class Error<out T>(
    /** Internal error code. */
    private val internalErrorCode: Int,

    ) : Resource<T>() {
    override val errorCode: Int
        get() = internalErrorCode
}
