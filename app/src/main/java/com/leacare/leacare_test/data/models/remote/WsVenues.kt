package com.leacare.leacare_test.data.models.remote

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


/**
 * Venues, as received from the web service.
 */
@Serializable
open class WsVenues {
    @SerialName("id")
    var id: String = ""

    @SerialName("name")
    var name: String = ""

    @SerialName("location")
    var location: Location? = null

    @SerialName("categories")
    var category: List<Category>? = null


    @Serializable
    class Category {
        @SerialName("shortName")
        var shortName: String = ""

        @SerialName("icon")
        var icon: Icon? = null
    }

    @Serializable
    class Icon {
        @SerialName("prefix")
        var prefixImageUrl: String = ""

        @SerialName("suffix")
        var suffixImageUrl: String = ""
    }
}

@Serializable
class Location {
    @SerialName("address")
    var address: String = ""

    @SerialName("crossStreet")
    var crossStreet: String = ""

    @SerialName("lat")
    var latitude: Double = 0.0

    @SerialName("lng")
    var longitude: Double = 0.0

    @SerialName("formattedAddress")
    var formattedAddress: List<String>? = null

}
