package com.leacare.leacare_test.data.models.remote

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class WsVenueDetailsResponse {
    @SerialName("venue")
    var venueDetails: WsVenueDetails? = null
}