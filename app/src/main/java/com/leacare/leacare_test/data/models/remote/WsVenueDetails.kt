package com.leacare.leacare_test.data.models.remote

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Venue details, as received from the web service.
 */
@Serializable
class WsVenueDetails : WsVenues() {
    @SerialName("verified")
    var verified: Boolean = false

    @SerialName("rating")
    var rating: Double = 0.0

    @SerialName("bestPhoto")
    var headerImage: BestPhoto? = null

    @SerialName("contact")
    var contact: Contact? = null

    @Serializable
    class Contact {
        @SerialName("formattedPhone")
        var phone: String = ""
    }

    @Serializable
    class BestPhoto {
        @SerialName("prefix")
        var prefix: String = ""

        @SerialName("suffix")
        var suffix: String = ""
    }

}