package com.leacare.leacare_test.data.source

import com.leacare.leacare_test.data.models.local.LeacareTestDataBase
import com.leacare.leacare_test.data.models.local.models.VenueEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Local data source, for Venue related objects.
 */
class FoursquareLocalDataSource @Inject constructor(
    /** Leacare  database. */
    private val leacareTestDataBase: LeacareTestDataBase
) {

    /**
     * Retrieves all saved venues.
     */
    fun getVenueData(): Flow<List<VenueEntity>> {
        return leacareTestDataBase.venueDao().getVenues()
    }

    /**
     * Insert venue.
     */
    suspend fun insertVenueData(venueEntity: VenueEntity) {
        return leacareTestDataBase.venueDao().insertVenue(venueEntity)
    }

    /**
     * Delete venue.
     */
    suspend fun deleteVenueData(venueId: String) {
        return leacareTestDataBase.venueDao().deleteVenue(venueId)
    }
}