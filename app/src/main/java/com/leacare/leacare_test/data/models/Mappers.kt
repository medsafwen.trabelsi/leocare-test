package com.leacare.leacare_test.data.models

import com.leacare.leacare_test.data.models.local.models.VenueEntity
import com.leacare.leacare_test.data.models.remote.WsVenueDetails
import com.leacare.leacare_test.data.models.remote.WsVenues
import com.leacare.leacare_test.domain.models.remote.VenueDetailsData
import com.leacare.leacare_test.domain.models.remote.VenuesData

/**
 * Converts a WsVenues instance into a VenuesData instance.
 */
fun WsVenues?.toVenuesData(): VenuesData? {
    return this?.let {
        val venuesData = VenuesData()
        venuesData.id = this.id
        venuesData.name = this.name
        venuesData.address = this.location?.formattedAddress?.joinToString(",") ?: ""
        venuesData.latitude = this.location?.latitude ?: 0.0
        venuesData.longitude = this.location?.longitude ?: 0.0
        venuesData.shortName = this.category?.firstOrNull()?.shortName ?: ""
        venuesData.urlImage =
            "${this.category?.firstOrNull()?.icon?.prefixImageUrl}512${this.category?.firstOrNull()?.icon?.suffixImageUrl}"

        venuesData
    }
}

/**
 * Converts a WsVenueDetails instance into a VenueDetailsData instance.
 */
fun WsVenueDetails?.toVenueDetails(): VenueDetailsData? {
    return this?.let {
        val venueDetailsData = VenueDetailsData()
        venueDetailsData.id = this.id
        venueDetailsData.name = this.name
        venueDetailsData.address = this.location?.formattedAddress?.joinToString(",") ?: ""
        venueDetailsData.latitude = this.location?.latitude ?: 0.0
        venueDetailsData.longitude = this.location?.longitude ?: 0.0
        venueDetailsData.shortName = this.category?.firstOrNull()?.shortName ?: ""
        venueDetailsData.urlImage = "${this.headerImage?.prefix}1000x300${this.headerImage?.suffix}"
        venueDetailsData.phone = this.contact?.phone ?: ""
        venueDetailsData.rating = this.rating.toFloat()
        venueDetailsData.verified = this.verified

        venueDetailsData
    }

}

/**
 * Converts a VenueEntity instance into a VenuesData instance.
 */
fun VenueEntity?.toVenue(): VenuesData? {
    return this?.let {
        val venueData = VenuesData()
        venueData.id = this.id
        venueData.name = this.name
        venueData.shortName = this.shortName
        venueData.urlImage = this.imageUrl
        venueData
    }

}

