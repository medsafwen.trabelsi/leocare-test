package com.leacare.leacare_test.data.models.local.models

import androidx.room.ColumnInfo
import androidx.room.Entity

/**
 * Venue database object.
 */
@Entity(tableName = "venue", primaryKeys = ["id"])
data class VenueEntity(
    /** Venue id. */
    @ColumnInfo(name = "id")
    val id: String,

    /** Venue name. */
    @ColumnInfo(name = "name")
    val name: String,

    /** Venue short name. */
    @ColumnInfo(name = "shortName")
    val shortName: String,

    /** Venue image url. */
    @ColumnInfo(name = "url")
    val imageUrl: String
)