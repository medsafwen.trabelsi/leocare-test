package com.leacare.leacare_test.data.models.remote

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Common web services responses container.
 */
@Serializable
data class WsResponse<T>(
        @SerialName("response")
        val data: T
)