package com.leacare.leacare_test.data.source

import android.content.Context
import com.leacare.leacare_test.data.models.ERROR_SOCKET_TIMEOUT
import com.leacare.leacare_test.data.models.ERROR_UNKNOWN
import com.leacare.leacare_test.data.models.HTTP_ERROR_BASE
import com.leacare.leacare_test.data.models.remote.Resource
import com.leacare.leacare_test.data.models.remote.WsVenueDetailsResponse
import com.leacare.leacare_test.data.models.remote.WsVenuesResponse
import com.leacare.leacare_test.data.service.FoursquareRetrofitService
import com.leacare.leacare_test.domain.models.CLIENT_ID
import com.leacare.leacare_test.domain.models.CLIENT_SECRET
import retrofit2.HttpException
import timber.log.Timber
import java.net.SocketTimeoutException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Foursquare related data network data source.
 */
class FoursquareRemoteDataSource @Inject constructor(
    /** Foursquare API service. */
    private val retrofitService: FoursquareRetrofitService,

    /** Context. */
    private val context: Context
) {
    /**
     * Retrieves the venues.
     */
    suspend fun fetchVenues(latitude: Double, longitude: Double): Resource<WsVenuesResponse> {
        return try {
            val currentDateFormat = SimpleDateFormat("YYYYMMdd", Locale.getDefault())

            Resource.success(
                retrofitService.getVenues(
                    clientId = CLIENT_ID,
                    clientSecret = CLIENT_SECRET,
                    version = currentDateFormat.format(Date()),
                    location = "$latitude,$longitude",
                    locale = context.resources.configuration.locale.language
                ).data
            )
        } catch (e: Exception) {
            Timber.w(e, "An exception occurred during the web service call")
            when (e) {
                is HttpException -> Resource.error(HTTP_ERROR_BASE + e.code())
                is SocketTimeoutException -> Resource.error(ERROR_SOCKET_TIMEOUT)
                else -> Resource.error(ERROR_UNKNOWN)
            }
        }
    }

    /**
     * Retrieves the venue details.
     */
    suspend fun fetchVenueDetails(venueId: String): Resource<WsVenueDetailsResponse> {
        return try {
            val currentDateFormat = SimpleDateFormat("YYYYMMdd", Locale.getDefault())

            Resource.success(
                retrofitService.getVenueDetails(
                    clientId = CLIENT_ID,
                    clientSecret = CLIENT_SECRET,
                    version = currentDateFormat.format(Date()),
                    venuesId = venueId,
                    locale = context.resources.configuration.locale.language
                ).data
            )
        } catch (e: Exception) {
            Timber.w(e, "An exception occurred during the web service call")
            when (e) {
                is HttpException -> Resource.error(HTTP_ERROR_BASE + e.code())
                is SocketTimeoutException -> Resource.error(ERROR_SOCKET_TIMEOUT)
                else -> Resource.error(ERROR_UNKNOWN)
            }
        }
    }
}