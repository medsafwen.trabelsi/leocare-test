package com.leacare.leacare_test.data.models.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.leacare.leacare_test.data.models.local.models.VenueEntity
import kotlinx.coroutines.flow.Flow

/**
 * DAO, for [VenueEntity] objects.
 */
@Dao
interface VenueDao {
    /**
     * Find the venues.
     */
    @Query("SELECT * FROM venue")
    fun getVenues(): Flow<List<VenueEntity>>

    /**
     * Insert venue.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVenue(venueEntity: VenueEntity)

    /**
     * Delete venue.
     */
    @Query("DELETE FROM venue WHERE id = :venueId")
    suspend fun deleteVenue(venueId: String)
}