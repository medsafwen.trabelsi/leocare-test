package com.leacare.leacare_test.data.models.remote

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class WsVenuesResponse {
    @SerialName("venues")
    var venuesList: List<WsVenues>? = null
}