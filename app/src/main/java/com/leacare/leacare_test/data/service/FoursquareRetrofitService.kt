package com.leacare.leacare_test.data.service

import com.leacare.leacare_test.data.models.remote.WsResponse
import com.leacare.leacare_test.data.models.remote.WsVenueDetailsResponse
import com.leacare.leacare_test.data.models.remote.WsVenuesResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Foursquare retrofit service declaration.
 */
interface FoursquareRetrofitService {

    @GET("venues/search")
    suspend fun getVenues(@Query("client_id") clientId: String, @Query("client_secret") clientSecret: String
                          ,@Query("v") version: String,@Query("ll") location: String,@Query("locale") locale: String): WsResponse<WsVenuesResponse>

    @GET("venues/{VENUE_ID}")
    suspend fun getVenueDetails(@Path("VENUE_ID") venuesId: String,@Query("client_id") clientId: String, @Query("client_secret") clientSecret: String
                          ,@Query("v") version: String,@Query("locale") locale: String): WsResponse<WsVenueDetailsResponse>

}