package com.leacare.leacare_test.domain.usescases

import com.leacare.leacare_test.domain.models.remote.VenueDetailsData
import com.leacare.leacare_test.domain.repositories.FoursquareRepository
import javax.inject.Inject

/**
 * FetchVenueDetailUsesCase venue details from api.
 */
class FetchVenueDetailUsesCase @Inject constructor(
    private val foursquareRepository: FoursquareRepository
) {
    suspend operator fun invoke(venueId: String): VenueDetailsData? {
        return foursquareRepository.fetchVenueDetails(venueId).dataOrNull
    }
}