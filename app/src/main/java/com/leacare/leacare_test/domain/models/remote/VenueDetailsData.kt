package com.leacare.leacare_test.domain.models.remote

/**
 * Venue details related data.
 */
class VenueDetailsData : VenuesData() {
    /** Venue phone. */
    var phone: String = ""

    /** Venue rating. */
    var rating: Float =0f

    /** Venue rating. */
    var verified: Boolean = false
}