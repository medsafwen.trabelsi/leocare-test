package com.leacare.leacare_test.domain.repositories

import com.leacare.leacare_test.data.models.local.models.VenueEntity
import com.leacare.leacare_test.data.models.remote.Resource
import com.leacare.leacare_test.domain.models.remote.VenueDetailsData
import com.leacare.leacare_test.domain.models.remote.VenuesData
import kotlinx.coroutines.flow.Flow

/**
 * Foursquare related repository.
 */
interface FoursquareRepository {
    /**
     * Retrieves venues from the foursquare api.
     *
     * @return a result, containing all venus.
     */
    suspend fun fetchVenues(latitude: Double, longitude: Double): Resource<List<VenuesData?>?>

    /**
     * Retrieves venue details from the foursquare api.
     *
     * @return a result, containing venue details.
     */
    suspend fun fetchVenueDetails(venueId: String): Resource<VenueDetailsData?>

    /**
     * Retrieves venue details from local data base.
     *
     * @return a result, containing venues.
     */
    suspend fun getVenues(): Flow<List<VenueEntity>>

    /**
     * Insert venue to local data base.
     *
     */
    suspend fun insertVenue(venueEntity: VenueEntity)

    /**
     * Delete venue to local data base.
     *
     */
    suspend fun deleteVenue(venueId: String)
}