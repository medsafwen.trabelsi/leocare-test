package com.leacare.leacare_test.domain.usescases

import com.leacare.leacare_test.data.models.local.models.VenueEntity
import com.leacare.leacare_test.domain.models.remote.VenueDetailsData
import com.leacare.leacare_test.domain.repositories.FoursquareRepository
import javax.inject.Inject

/**
 * AddVenueUseCase add venue to local data base.
 */
class AddVenueUseCase @Inject constructor(
    private val foursquareRepository: FoursquareRepository
) {
    suspend operator fun invoke(venueDetailsData: VenueDetailsData) {
        return foursquareRepository.insertVenue(
            VenueEntity(
                id = venueDetailsData.id,
                name = venueDetailsData.name,
                shortName = venueDetailsData.shortName,
                imageUrl = venueDetailsData.urlImage
            )
        )
    }
}