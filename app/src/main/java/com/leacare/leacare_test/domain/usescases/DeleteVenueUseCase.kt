package com.leacare.leacare_test.domain.usescases

import com.leacare.leacare_test.domain.repositories.FoursquareRepository
import javax.inject.Inject

/**
 * DeleteVenueUseCase delete venue from local data base.
 */
class DeleteVenueUseCase @Inject constructor(
    private val foursquareRepository: FoursquareRepository
) {
    suspend operator fun invoke(venueId: String) {
        return foursquareRepository.deleteVenue(venueId)
    }
}