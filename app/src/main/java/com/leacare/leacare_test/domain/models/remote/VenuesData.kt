package com.leacare.leacare_test.domain.models.remote

/**
 * Venues related data.
 */
open class VenuesData {
    /** Venue id. */
    var id: String = ""

    /** Venue name. */
    var name: String = ""

    /** Venue latitude. */
    var latitude: Double = 0.0

    /** Venue longitude. */
    var longitude: Double = 0.0

    /** Venue address. */
    var address: String = ""

    /** Venue short name. */
    var shortName: String = ""

    /** Venue url image. */
    var urlImage: String = ""

}