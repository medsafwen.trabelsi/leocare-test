package com.leacare.leacare_test.domain.usescases

import com.leacare.leacare_test.data.models.local.models.VenueEntity
import com.leacare.leacare_test.domain.repositories.FoursquareRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * GetVenuesUseCase get all venues from local data base.
 */
class GetVenuesUseCase @Inject constructor(
    private val foursquareRepository: FoursquareRepository
) {
    suspend operator fun invoke(): Flow<List<VenueEntity>> {
        return foursquareRepository.getVenues()
    }
}