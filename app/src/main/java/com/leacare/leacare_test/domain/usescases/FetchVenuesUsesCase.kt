package com.leacare.leacare_test.domain.usescases

import com.leacare.leacare_test.domain.models.remote.VenuesData
import com.leacare.leacare_test.domain.repositories.FoursquareRepository
import javax.inject.Inject

/**
 * FetchVenuesUsesCase fetch venues from api.
 */
class FetchVenuesUsesCase @Inject constructor(
    private val foursquareRepository: FoursquareRepository
) {
    suspend operator fun invoke(latitude: Double, longitude: Double): List<VenuesData?>? {
        return foursquareRepository.fetchVenues(latitude,longitude).dataOrNull
    }
}